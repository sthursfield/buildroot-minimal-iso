# Buildroot Minimal ISO

This is a Buildroot project to build a minimal x86_64 system image that can boot
in [openQA](http://open.qa/).

It is used to test the [ssam_openqa](https://gitlab.gnome.org/sthursfield/ssam_openqa/) tool.
The image should be small enough to commit directly to a Git repo (~10MB).

To set up the build, run:

    make qemu_x86_64_minimal_iso_defconfig

To build the image, run:

    make

The output will be in `output/images/root.iso9660`.

To change the image configuration. run:

    make menuconfig

To save the updated configuration, run:

    make savedefconfig BR2_DEFCONFIG=configs/qemu_x86_64_minimal_iso_defconfig

See the [Buildroot manual](https://buildroot.org/downloads/manual/manual.html) for more
information.
